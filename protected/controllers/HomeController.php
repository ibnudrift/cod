<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}

	public function actionCreatecategory()
	{
		for ($i=1; $i < 111; $i++) { 
			$model = new PrdCategoryProduct;
			$model->category_id = 12;
			$model->product_id = $i;
			$model->save(false);
		}
	}

	public function actionInput()
	{
		$data = Table61::model()->findAll();
		foreach ($data as $key => $value) {
			$model = new PrdProduct;
			$model->kode = $value->col_1;
			if ($value->col_7 != '') {
				copy(YiiBase::getPathOfAlias('webroot').'/images/precise/'.$value->col_7.'/COVER.jpg', YiiBase::getPathOfAlias('webroot').'/images/product/'.Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-cover.jpg');
				$model->image = Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-cover.jpg';
			}else{
				$model->image = '';
			}
			$model->harga = $value->col_6;
			$model->harga_coret = 0;
			$model->save(false);
			$dataDesc = new PrdProductDescription;
			$dataDesc->product_id = $model->id;
			$dataDesc->language_id = 2;
			$dataDesc->name = $value->col_2;
			$dataDesc->subtitle = $value->col_3;
			$dataDesc->desc = '<p>'.$value->col_4.'</p>';
			$dataDesc->save(false);
			$dataAttr = explode(',', $value->col_5);
			foreach ($dataAttr as $v) {
				$modelAttr = new PrdProductAttributes;
				$modelAttr->product_id = $model->id;
				$modelAttr->attribute = trim($v);
				$modelAttr->stock = 10;
				$modelAttr->price = $value->col_6;
				$modelAttr->save(false);
				$modelAttr->id_str = $modelAttr->id;
				$modelAttr->save(false);

			}
			if ($value->col_7 != '') {
				for ($i=1; $i < 7; $i++) { 
					$modelImage = new PrdProductImage;
					$modelImage->product_id = $model->id;
					copy(YiiBase::getPathOfAlias('webroot').'/images/precise/'.$value->col_7.'/'.$i.'.jpg', YiiBase::getPathOfAlias('webroot').'/images/product/'.Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-photo'.$i.'.jpg');
					$modelImage->image = Slug::create($value->col_1).'-'.Slug::create($value->col_2).'-photo'.$i.'.jpg';
					$modelImage->save(false);
				}
			}



		}
	}

	public function actionIndex()
	{
		// $criteria2=new CDbCriteria;
			// $criteria2->with = array('description');
			// $criteria2->order = 'date DESC';
			// $criteria2->addCondition('status = "1"');
			// $criteria2->addCondition('description.language_id = :language_id');
			// $criteria2->params[':language_id'] = $this->languageID;

			// if ($_GET['category']) {
			// 	$criteria = new CDbCriteria;
			// 	$criteria->with = array('description');
			// 	$criteria->addCondition('t.id = :id');
			// 	$criteria->params[':id'] = $_GET['category'];
			// 	$criteria->addCondition('t.type = :type');
			// 	$criteria->params[':type'] = 'category';
			// 	$criteria->order = 'sort ASC';
			// 	$strCategory = PrdCategory::model()->find($criteria);

			// 	// $inArray = PrdProduct::getInArrayCategory($_GET['category']);
			// 	// $criteria2->addInCondition('t.category_id', $inArray);
			// 	$criteria2->addCondition('t.tag LIKE :category');
			// 	$criteria2->params[':category'] = '%category='.$_GET['category'].',%';
			// }else{
			// 	$criteria2->addCondition('t.tag LIKE :category');
			// 	$criteria2->params[':category'] = '%category=35,%';
			// }
			// $pageSize = 8;

			// $product = new CActiveDataProvider('PrdProduct', array(
			// 	'criteria'=>$criteria2,
			//     'pagination'=>array(
			//         'pageSize'=>$pageSize,
			//     ),
			// ));

			// $model = new ContactForm;
			// $model->scenario = 'insert';

		$this->layout='//layouts/column1';
		
		$this->render('index', array(
			// 'product'=>$product,
			// 'model'=>$model,
		));
	}

	public function actionCategory()
	{
		$this->layout='//layouts/column2';

		$this->render('category', array(
		));
	}

	public function actionProduct()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Product - '. $this->pageTitle;
		
		$this->render('product', array());
	}

	public function actionProductDetail()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Product Detail - '. $this->pageTitle;
		
		$this->render('product_detail', array(	
		));
	}
	public function actionSyarat()
	{
		$this->layout='//layouts/column2';

		$this->render('syarat_ketentuan', array(
		));
	}

	public function actionProcess()
	{
		$this->layout='//layouts/column2';
		
		$this->render('process', array(
		));
	}

	public function actionFaq()
	{
		$this->layout='//layouts/column2';

		$criteria=new CDbCriteria;
		$criteria->with = array('description');
		$criteria->order = 't.id ASC';

		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		$pageSize = 100000;

		$dataFaq = new CActiveDataProvider('Faq', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		$this->render('faq', array(
			'dataFaq'=>$dataFaq,
		));
	}

	public function actionError()
	{
		$this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{

				$criteria=new CDbCriteria;
				$criteria->with = array('description', 'category', 'categories');
				$criteria->order = 'date DESC';
				$criteria->addCondition('status = "1"');
				$criteria->addCondition('terlaris = "1"');
				$criteria->addCondition('description.language_id = :language_id');
				$criteria->params[':language_id'] = $this->languageID;
				$pageSize = 12;
				$criteria->group = 't.id';
				$product = new CActiveDataProvider('PrdProduct', array(
					'criteria'=>$criteria,
				    'pagination'=>array(
				        'pageSize'=>$pageSize,
				    ),
				));


				$criteria = new CDbCriteria;
				$criteria->with = array('description');
				$criteria->addCondition('t.parent_id = :parent_id');
				$criteria->params[':parent_id'] = 0;
				$criteria->addCondition('t.type = :type');
				$criteria->params[':type'] = 'category';
				$criteria->limit = 3;
				$criteria->order = 'sort ASC';
				$categories = PrdCategory::model()->findAll($criteria);

				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;
				$this->render('error', array(
					'error'=>$error,
					'product'=>$product,
					'categories'=>$categories,
				));
			}
		}

	}

	public function actionAbout()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('about', array(	
		));
	}

	public function actionLocation()
	{
		$this->pageTitle = 'Our Location - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('location', array(	
		));
	}

	public function actionTraces()
	{
		$this->pageTitle = 'Trace Your - '.$this->pageTitle;
		$this->layout='//layouts/column3';

		$this->render('traces', array(	
		));
	}

	public function actionSnap()
	{
		$this->pageTitle = 'Snap and Share - '.$this->pageTitle;
		$this->layout='//layouts/column3';

		$this->render('snap_share', array(	
		));
	}

	public function actionContact()
	{
		$this->pageTitle = 'Contact - '.$this->pageTitle;
		$this->layout='//layouts/column2';
		
		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			if (!isset($_POST['g-recaptcha-response'])) {
				$this->redirect(array('/home/contactus'));
	        }
	        
	        $secret_key = "6LfaqgkUAAAAAEGhdM7GVk6o-jNbidJ9t3xgc0wn";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          $model->addError('verifyCode', 'Silahkan verifikasi captcha yang tersedia');
	        }

			$model->attributes=$_POST['ContactForm'];
			if(!$model->hasErrors() AND $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email']),
					'subject'=>'Hi, Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for sending application career us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$this->render('contact', array(	
			'model'=>$model,
		));
	}

}


