<?php
$this->breadcrumbs=array(
	'Traces'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Traces','url'=>array('index')),
	array('label'=>'Add Traces','url'=>array('create')),
);
?>

<h1>Manage Traces</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'traces-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'kode',
		'origin',
		'company',
		'date_process',
		'date_distribution',
		/*
		'date_input',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
