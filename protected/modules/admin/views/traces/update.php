<?php
$this->breadcrumbs=array(
	'Traces'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-minus',
	'title'=>'Traces',
	'subtitle'=>'Edit Traces',
);

$this->menu=array(
	array('label'=>'List Traces', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Traces', 'icon'=>'plus-sign','url'=>array('create')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>