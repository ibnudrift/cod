<?php
$this->breadcrumbs=array(
	'Traces'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Traces', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Traces', 'icon'=>'plus-sign','url'=>array('create')),
	array('label'=>'Edit Traces', 'icon'=>'pencil','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Traces', 'icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
);
?>

<h1>View Traces #<?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'kode',
		'origin',
		'company',
		'date_process',
		'date_distribution',
		'date_input',
	),
)); ?>
