<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'traces-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="widget">
<h4 class="widgettitle">Data Traces</h4>
<div class="widgetcontent">

	<?php echo $form->textFieldRow($model,'kode',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'origin',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'company',array('class'=>'span5','maxlength'=>225)); ?>

	<?php echo $form->textFieldRow($model,'date_process',array('class'=>'span5 datepicker')); ?>

	<?php echo $form->textFieldRow($model,'date_distribution',array('class'=>'span5 datepicker')); ?>

	<?php // echo $form->textFieldRow($model,'date_input',array('class'=>'span5')); ?>

	<?php 
	$urls_full = Yii::app()->request->hostInfo . Yii::app()->request->baseUrl;;

	$full_url_qr = 'http://api.qrserver.com/v1/create-qr-code/?size=150x150&data='. $urls_full.'/home/traces?origin='. $model->origin;
	?>
	<?php if ($model->scenario == 'update'): ?>
		<div class="control-group ">
		    <label class="control-label" for="Traces_date_distribution">&nbsp;</label>
		    <div class="controls">
		        <a href="<?php echo $full_url_qr ?>">
		        	<img style="max-width: 80px;" src="<?php echo $full_url_qr ?>" alt="">
		        </a>
		        <div class="clearfix" style="display: block; clear: both;"></div>
		        <span><?php echo $full_url_qr ?></span>
		    </div>
		</div>
	<?php endif ?>


		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Add' : 'Save',
		)); ?>
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			// 'buttonType'=>'submit',
			// 'type'=>'info',
			'url'=>CHtml::normalizeUrl(array('index')),
			'label'=>'Batal',
		)); ?>
</div>
</div>
<div class="alert">
  <button type="button" class="close" data-dismiss="alert">×</button>
  <strong>Warning!</strong> Fields with <span class="required">*</span> are required.
</div>

<?php $this->endWidget(); ?>
