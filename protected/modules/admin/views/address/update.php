<?php
$this->breadcrumbs=array(
	'Location'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->pageHeader=array(
	'icon'=>'fa fa-bank',
	'title'=>'Location',
	'subtitle'=>'Edit Location',
);

$this->menu=array(
	array('label'=>'List Location', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Location', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Location', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>