<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>ill-about.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Blogs & Articles</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blogs & Articles</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a href="#">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="home-sec-blog">
  <div class="prelative container">
      <div class="py-4"></div>
      
      <div class="judul titles_pg">
          <h4>Latest Blog & Articles</h4>
      </div>     

      <div class="py-3"></div>

      <div class="box-image outers_list_news_data">
         <div class="row no-gutters lists_data">
            <?php for ($i=0; $i < 5; $i++) { ?>
            <div class="col-md-20 col-30">
              <div class="items">
                 <div class="pictures">
                  <a href="#">
                    <img src="<?php echo $this->assetBaseurl; ?>Layer-26.jpg" class="img img-fluid" alt="">
                  </a>
                 </div>
                 <div class="contents">
                    <a href="#"><p>Peresmian pabrik baru BMI untuk pemrosesan udang di Lamongan</p></a>
                    <a href="#" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
            <div class="col-md-20 col-30">
              <div class="items">
                 <div class="pictures">
                  <a href="#">
                    <img src="<?php echo $this->assetBaseurl; ?>Layer-25.jpg" class="img img-fluid" alt="">
                  </a>
                 </div>
                 <div class="contents">
                    <a href="#"><p>Indonesia: Kekuatan sumbangsih hasil laut negara meningkat pesa</p></a>
                    <a href="#" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
            <div class="col-md-20 col-30">
                <div class="items">
                 <div class="pictures">
                  <a href="#">
                    <img src="<?php echo $this->assetBaseurl; ?>Layer-24.jpg" class="img img-fluid" alt="">
                  </a>
                 </div>
                 <div class="contents">
                    <a href="#"><p>Mempelajari aneka penyakit yang dapat mengurangi nilai jual uda</p></a>
                    <a href="#" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
            <?php } ?>
         </div>

      </div>
      <div class="clear clearfix"></div>

      <div class="clear clearfix"></div>
    </div>
</section>