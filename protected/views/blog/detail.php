<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>ill-about.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Blogs & Articles</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Blogs & Articles</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a href="#">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blog-detail-cnt1">
    <div class="prelatife container w904">
      <div class="inners py-5">
        <div class="tops_title text-center pb-4 mb-2">
          <p>BLOGS & ARTICLES</p>
          <div class="py-2"></div>
          <h1>Peresmian pabrik baru BMI untuk pemrosesan udang
          di Lamongan</h1>
        </div>
        <div class="py-1"></div>

        <div class="inner-img">
          <img src="<?php echo $this->assetBaseurl ?>Layer-45.jpg" class="img img-fluid" alt="">
        </div>
        <div class="inner-isi content-texts pt-4 mt-3">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce orci augue, tincidunt eu placerat quis, varius sit amet enim. Sed nisl orci, tempor ornare ligula eget, bibendum ullamcorper sem. In at elementum odio, quis gravida massa. Phasellus ultricies ornare mattis. Maecenas ultricies venenatis orci vitae mattis. Aenean viverra felis in nibh suscipit lacinia. Sed odio neque, mattis varius tempor quis, facilisis vitae metus. Proin fermentum condimentum tortor, vel rutrum lorem euismod et. Donec non efficitur erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Phasellus aliquam nibh vitae euismod faucibus. In sollicitudin porta malesuada. Nulla facilisi.</p>
          <p>Mauris id luctus nunc. Quisque vel dictum erat, nec tincidunt mi. Ut bibendum dolor eu augue feugiat, efficitur varius sapien sodales. Nam neque turpis, fermentum ut interdum sed, sodales ac sem. Cras vitae nibh vitae ante accumsan commodo. Nam scelerisque feugiat erat ac lacinia. Donec sagittis mauris urna, sit amet egestas risus imperdiet eu. Suspendisse sed consectetur tortor, et feugiat erat. Cras vestibulum nunc neque. Vestibulum gravida elit nulla, a viverra magna volutpat sit amet. Aenean ante lorem, dictum sed lorem sit amet, pharetra rhoncus purus. Aliquam imperdiet sem eu dui luctus tincidunt. Cras sit amet consequat ipsum. In id odio consequat, tristique massa id, consectetur dolor. Duis maximus pellentesque orci id pellentesque.</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at quam id ante auctor volutpat. Donec blandit justo sed sapien scelerisque porta. Praesent vitae nisl lectus. Integer turpis odio, pellentesque vitae diam sit amet, vehicula dictum justo. Aenean lorem neque, mollis facilisis dictum a, suscipit at mi. Nam pharetra quam eget turpis tristique mattis. In mollis nisi massa, id facilisis quam suscipit id.</p>
          <p>Duis ac aliquet magna. Mauris vel varius velit. Sed molestie mattis eros. Aenean ut risus at urna pharetra fringilla. Nunc imperdiet neque elementum, lacinia elit non, rhoncus velit. Proin suscipit convallis volutpat. Vestibulum maximus est quis finibus posuere. Nullam id feugiat eros. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
          <p>Curabitur ultricies justo et neque porttitor ultricies. Vestibulum venenatis ligula quis venenatis placerat. Nulla consequat feugiat posuere. Duis eu porttitor turpis. Ut odio urna, sodales eu risus at, vestibulum rhoncus diam. Nulla eget quam erat. Aliquam sed purus dapibus sapien malesuada sodales.</p>

          <div class="text-center">
            <div class="py-3"></div>
            <div class="tengah buttons_blue d-block mx-auto">
              <a href="#" class="btn btn-info btns_blue_host">Next Blogs & Articles</a>
            </div>
          </div>
        </div>
      </div>
      <div class="py-3"></div>
    </div>
  </section>
  <div class="py-3"></div>
  <div class="lines-grey"></div>
  <div class="py-5"></div>  

  <section class="home-sec-3 outers_block_articleshome mt-3">
   <div class="prelative container">
      <h4>Others Blog & Articles</h4>
      <div class="py-3 d-block d-sm-none"></div>
      <div class="box-image outers_list_news_data">
         <div class="row no-gutters lists_data">
            <div class="col-md-20 col-30">
              <div class="items">
                 <div class="pictures">
                    <img src="<?php echo $this->assetBaseurl; ?>Layer-26.jpg" class="img img-fluid" alt="">
                 </div>
                 <div class="contents">
                    <p>Peresmian pabrik baru BMI untuk pemrosesan udang di Lamongan</p>
                    <a href="#" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
            <div class="col-md-20 col-30">
              <div class="items">
                 <div class="pictures">
                    <img src="<?php echo $this->assetBaseurl; ?>Layer-25.jpg" class="img img-fluid" alt="">
                 </div>
                 <div class="contents">
                    <p>Indonesia: Kekuatan sumbangsih hasil laut negara meningkat pesa</p>
                    <a href="#" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
            <div class="col-md-20 col-30">
                <div class="items">
                 <div class="pictures">
                    <img src="<?php echo $this->assetBaseurl; ?>Layer-24.jpg" class="img img-fluid" alt="">
                 </div>
                 <div class="contents">
                    <p>Mempelajari aneka penyakit yang dapat mengurangi nilai jual uda</p>
                    <a href="#" class="view_art">View Article</a>
                 </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<div class="py-4"></div>
<div class="py-3"></div>

<?php
/*
<section class="outers_page_static back_cream mh500 back_grey_pattern">
  <div class="insides sub_page_static about_us">
    <div class="prelatife container">
      <div class="clear height-50"></div><div class="height-10"></div>

      <div class="content-text insides_static">
        <h1 class="title_page">BLOGS</h1>
        <div class="clear"></div>
        <h3 class="tagline"><?php echo $dataBlog->description->title ?></h3>
        <div class="clear"></div>
        <div class="row details_cont_articles">
          <div class="col-md-9 text-left">
            <div class="left_cont">
              <div class="mw906">

                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(980,1000, '/images/blog/'.$dataBlog->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="">

                <?php echo $dataBlog->description->content ?>

                <div class="clear height-10"></div>
                <div class="shares-text text-left p_shares_article">
                    <span class="inline-t">SHARE</span>&nbsp; / &nbsp;<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=#">FACEBOOK</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://plus.google.com/share?url=#">GOOGLE PLUS</a>&nbsp; /
                    &nbsp;<a target="_blank" href="https://twitter.com/home?status=#">TWITTER</a>
                </div>

                <div class="clear"></div>
              </div>
            </div>

          </div>
          <div class="col-md-3 text-left">
            <div class="right_cont">
              <div class="padding-left-25">
                  <span class="sub_page_title">Other Blogs</span>
              </div>
              <div class="clear"></div>
              <div class="right_sub_menu">
                <ul class="list-unstyled">
                    <?php foreach ($dataBlogs->getData() as $key => $value): ?>
                        
                  <li><a href="<?php echo CHtml::normalizeUrl(array('detail', 'id'=>$value->id)); ?>"><?php echo $value->description->title ?></a></li>
                    <?php endforeach ?>
                </ul>
                <div class="clear"></div>
              </div>

              <div class="clear"></div>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        

        <div class="clear"></div>
      </div>
      
      <div class="clear height-20"></div>
      <div class="clear height-50"></div>
    </div>
    <div class="clear"></div>
  </div>
</section>
*/
?>