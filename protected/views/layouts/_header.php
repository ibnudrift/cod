<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>
<header class="head <?php if ($active_menu_pg != 'home/index'): ?>insides-head<?php endif; ?>">
  <div class="prelative container mx-auto">
    <div class="row">
      <div class="col-md-45 col-45">
        <div class="py-1"></div>
        <div class="lgo_header">
          <a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">
          <img src="<?php echo $this->assetBaseurl; ?>logo-header.png" alt="<?php echo Yii::app()->name; ?>" class="img img-fluid">
          </a>
        </div>
        <div class="taglines_hd pl-2">
          <img src="<?php echo $this->assetBaseurl; ?>nmid_tagline_header.png" alt="<?php echo 'tagline '.Yii::app()->name; ?>" class="img img-fluid">
        </div>
      </div>
      <div class="col-md-15 col-15">
        <div class="py-2 my-1"></div>
        <div class="text-right tops_menu_linehd">
          <a href="javascript:;" class="showmenu_barresponsive"><img class="img img-fluid" src="<?php echo $this->assetBaseurl; ?>bc_menutag.png" alt=""></a>
          <div class="clear clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="outer-blok-black-menuresponss-hides" style="display: none;">
  <div class="prelatife container">
    <div class="clear height-45"></div>
    <div class="float-right mb-3">
      <div class="hidesmenu-frightd"><a href="#" class="closemrespobtn"><img src="<?php echo $this->assetBaseurl ?>closen-btn.png" alt=""></a></div>
    </div>
    <div class="blocksn_logo-centers">
      <img src="<?php echo $this->assetBaseurl ?>lgo-headers_wild.png" alt="logo" class="img img-fluid d-block mx-auto">
    </div>
    <div class="height-30 hidden-xs"></div>
    <div class="menu-sheader-datals py-4">
      <ul class="list-unstyled">
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/location')); ?>">Location</a></li>
        <!-- <li><a href="<?php // echo CHtml::normalizeUrl(array('/home/traces')); ?>">Trace Origin</a></li> -->
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/snap')); ?>">Snap And Share</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
      </ul>
    </div>
    <div class="blocks-info-menubtm d-block mx-auto">
      <span>Need Information?</span>
      <div class="py-2 my-1"></div>
      <div class="row justify-content-center">
        <div class="col-md-30 col-sm-30 col-30">
          <i class="fa fa-whatsapp"></i>
          <div class="py-1"></div>
          <p>Click to Whatsapp Chat<br>
          <a href="https://wa.me/<?php echo str_replace('08', '628', str_replace(' ', '', $this->setting['contact_wa'])); ?>"><?php echo $this->setting['contact_wa'] ?></a></p>
        </div>
        <div class="col-md-30 col-sm-30 col-30">
          <i class="fa fa-envelope-o"></i>
          <div class="py-1"></div>
          <p>Send Us Email <br>
          <a target="_blank" href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></p>
        </div>
      </div>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
  $(function(){
    // show and hide menu responsive
    $('a.showmenu_barresponsive').on('click', function() {
      $('.outer-blok-black-menuresponss-hides').slideToggle('slow');
      return false;
    });
    $('a.closemrespobtn').on('click', function() {
      $('.outer-blok-black-menuresponss-hides').slideUp('slow');
      return false;
    });

  });
</script>