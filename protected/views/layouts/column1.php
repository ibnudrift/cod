<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<!-- Start fcs -->
<?php
    $criteria=new CDbCriteria;
    $criteria->with = array('description');
    $criteria->addCondition('description.language_id = :language_id');
    $criteria->addCondition('active = 1');
    $criteria->params[':language_id'] = $this->languageID;
    $criteria->group = 't.id';
    $criteria->order = 't.id ASC';
    $slide = Slide::model()->with(array('description'))->findAll($criteria);
?>
  <div class="fcs-wrapper outers_fcs_wrapper prelatife wrapper-slide">
        <div id="myCarousel_home" class="carousel slide" data-ride="carousel" data-interval="5500">
            <!-- <div class="cols_buttons_nav">
                <div class="prelative container">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel_home" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel_home" data-slide-to="1"></li>
                  </ol>
                </div>
            </div> -->
            <div class="carousel-inner">
                <?php foreach ($slide as $key => $value): ?>
                <div class="carousel-item <?php echo ($key == 0)? 'active':'' ?> home-slider-new">
                    <img class="w-100 d-none d-sm-block" src="<?php echo Yii::app()->baseUrl.'/images/slide/'. $value->image; ?>" alt="" style="background-repeat: no-repeat !important; background-size: cover !important; background-position: center;">
                    <img class="w-100 d-block d-sm-none" src="<?php echo Yii::app()->baseUrl.'/images/slide/'. $value->image2; ?>" alt="">
                </div>
                <?php endforeach ?>
            </div>
            <!-- <div class="carousel-caption caption-slider-home">
                <div class="prelative container sliderrrr">
                    <div class="row">
                        <div class="col-md-32"></div>
                        <div class="col-md-28">
                            <div class="bxsl_tx_fcs">
                                <div class="content text-left">
                                    <h2 class="t1">Leading The Indonesian Shrimp Industries Since 1981</h2>
                                    <p>We are proud of the consistently high quality of the shrimp and other seafood products we offer. We help manage and accompany shrimp farmers, giving the best nurturing advise, observe when they are harvesting and delivered to our processing factories.</p>
                                </div>
                            </div>                           
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div> -->
            
        </div>
</div>

<?php echo $content ?>

<?php echo $this->renderPartial('//layouts/_footer', array('view_section'=> true)); ?>

<script type="text/javascript">
    $(document).ready(function(){
        
        if ($(window).width() >= 768) {
            var $item = $('#myCarousel_home.carousel .carousel-item'); 
            // var $wHeight = $(window).height() - 80;
            var $wHeight = $(window).height();
            $item.eq(0).addClass('active');
            $item.height($wHeight); 
            $item.addClass('full-screen');

            $('#myCarousel_home.carousel img.d-none.d-sm-block').each(function() {
              var $src = $(this).attr('src');
              var $color = $(this).attr('data-color');
              $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-color' : $color
              });
              $(this).remove();
            });

            $(window).on('resize', function (){
              $wHeight = $(window).height();
              $item.height($wHeight);
            });

            $('#myCarousel_home.carousel').carousel({
              interval: 4000,
              pause: "false"
            });
        }

    });
</script>

<script>
    $(document).ready(function (){
        $("#click").click(function (){
            $('html, body').animate({
                scrollTop: $("#div1").offset().top
            }, 2000);
        });
    });
</script>

<style type="text/css">
    .fcs-wrapper.outers_fcs_wrapper .carousel .carousel-inner .carousel-item{
        background-position: center !important;
    }
</style>
<?php $this->endContent(); ?>
