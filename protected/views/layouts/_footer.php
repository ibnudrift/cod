<?php if ($view_section === true): ?>
    <section class="bottoms_home_block_pop">
      <div class="prelatife container">
        <div class="picts_onleft">
          <img src="<?php echo $this->assetBaseurl ?>banners_left_section3.png" alt="" class="img img-fluid">
        </div>
        <div class="inners d-block mx-auto text-center">
          <div class="py-5"></div>
          <div class="py-3"></div>
          <p>Snap and share your love for Wildskin</p>
          <h4>AND GET A REWARDING PRIZE EVERY MONTH!</h4>
          <div class="py-2"></div>
          <a href="<?php echo CHtml::normalizeUrl(array('/home/snap')); ?>" class="btn btn_customs_default mx-auto">FIND OUT HOW</a>
          <div class="celar"></div>
        </div>
        <div class="picts_onright">
          <img src="<?php echo $this->assetBaseurl ?>banners_right_section3.png" alt="" class="img img-fluid">
        </div>
      </div>
    </section>
<?php else: ?>
    <div class="lines_cream_footer"></div>
<?php endif ?>

<footer class="foot back-white">
    <div class="prelatife container">
        <div class="inners_text">
            <div class="py-3"></div>
            <div class="row text-center">
                <div class="col-md-5"></div>
                <div class="col-md-50">
                    <p>Wildskin Salted Egg Fish Skin or Keripik Kulit Ikan dengan Bumbu Rasa Telur Asin is a natural source of Vitamin B12. <br>
                    Wildskin Alaskan Cod Salted Egg Fish Skin is manufactured by PT. Bumi Menara Internusa.</p>
                </div>
                <div class="col-md-5"></div>
            </div>
            <div class="py-4 d-none d-sm-block"></div>

            <div class="row">
                <div class="col-md-20">
                    <div class="py-2"></div>
                    <div class="blocks_socmed_ft">
                        <span>Follow us on</span> <br>
                        <div class="py-1"></div>
                        <p>
                            <a target="_blank" href="https://pinterest.com/<?php echo $this->setting['url_pinterest'] ?>"><i class="fa fa-pinterest"></i></a>
                            &nbsp;&nbsp;
                            <a target="_blank" href="https://facebook.com/<?php echo $this->setting['url_facebook'] ?>"><i class="fa fa-facebook-f"></i></a>
                            &nbsp;&nbsp;
                            <a target="_blank" href="https://instagram.com/<?php echo $this->setting['url_instagram'] ?>"><i class="fa fa-instagram"></i></a>
                        </p>
                    </div>
                </div>
                <div class="col-md-20">
                    <div class="lgo_footers d-block mx-auto text-center">
                        <a href="#"><img src="<?php echo $this->assetBaseurl ?>lgons_footer_middle.jpg" alt="" class="img img-fluid mx-auto"></a>
                    </div>
                </div>
                <div class="col-md-20">
                    <div class="blocks_socmed_ft text-right">
                        <span><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></span> <br>
                        <div class="py-1"></div>
                        <p>
                            <a href="https://wa.me/<?php echo str_replace('08', '628', str_replace(' ', '', $this->setting['contact_wa'])); ?>">WA <?php echo $this->setting['contact_wa'] ?></a>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a>
                        </p>
                    </div>
                    
                </div>
            </div>

            <div class="py-4 d-none d-sm-block"></div>
            <div class="py-2 d-block d-sm-none"></div>

            <div class="row">
                <div class="col-md-60">
                    <div class="t-copyrights text-center">
                        <p>
                        &copy; COPYRIGHT 2020 - Wildskin Alaskan Cod Salted Egg Fish Skin. <br>
                        Website design & development by <a href="https://www.markdesign.net/" target="_blank" title="Website Design Surabaya">Mark Design Indonesia</a>.</p>
                    </div>
                </div>
            </div>
            <div class="py-3"></div>

            <div class="clear"></div>
        </div>
    </div>
</footer>