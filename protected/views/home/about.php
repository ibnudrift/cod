
<section class="outer_wrapper_inside">
    <section class="about-snt-1">
        <div class="tops_page_title">
            <h1>ABOUT WILDSKIN</h1>
        </div>

        <div class="py-4"></div>
        <div class="py-2"></div>

        <div class="prelatife container">
            <div class="inners">
                <div class="content-texts text-center mx-auto mw845">
                    <!-- <h2>WHAT SETS US APART<br>FROM ANOTHER FRIED FISH SKIN PRODUCT</h2> -->
                    <img src="<?php echo $this->assetBaseurl ?>txt-title-about.png" alt="" class="img img-fluid">
                    <p>&nbsp;</p>
                   <?php echo $this->setting['about1_content'] ?>

                    <div class="py-4"></div>
                    <div class="py-2"></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-snt-2">
        <div class="prelatife container">
            <div class="inners">
                <div class="content-texts text-center mx-auto">
                    <div class="text-center">
                        <img src="<?php echo $this->assetBaseurl ?>subtitle-about-2.png" alt="" class="img img-fluid">
                    </div>
                    <div class="py-4"></div>
                    <div class="py-2"></div>

                    <?php 
                    $arr_icon = array(
                                    array(
                                        'pict'=> $this->setting['about2_child_pict_1'],
                                        'names'=> $this->setting['about2_child_title_1'],
                                    ),
                                    array(
                                        'pict'=> $this->setting['about2_child_pict_2'],
                                        'names'=> $this->setting['about2_child_title_2'],
                                    ),
                                    array(
                                        'pict'=> $this->setting['about2_child_pict_3'],
                                        'names'=> $this->setting['about2_child_title_3'],
                                    ),
                                    array(
                                        'pict'=> $this->setting['about2_child_pict_4'],
                                        'names'=> $this->setting['about2_child_title_4'],
                                    ),
                                    
                                );
                    ?>
                    <div class="lists_sub_icon_natural">
                        <div class="row">
                            <?php foreach ($arr_icon as $key => $value): ?>
                            <div class="col-md-15">
                                <div class="items mx-auto d-block">
                                    <div class="picts icon"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $value['pict'] ?>" alt="" class="img-fluid mx-auto d-block"></div>
                                    <div class="desc_info d-block mx-auto">
                                        <span><?php echo $value['names'] ?></span>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach ?>

                        </div>
                    </div>
                    <!-- End list sub icon -->
                    
                    <div class="clear"></div>
                </div>
                <div class="py-3 d-block d-sm-none"></div>

                <div class="py-5"></div>
                <div class="py-2"></div>

                <div class="box_mx_middles about_ns2 prelatife d-none d-sm-block">
                    <div class="ins_text">
                        <h5>GET YOUR SALTED EGG FISH SKIN NOW!</h5>
                        <div class="clear"></div>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['contact_tokopedia'] ?>"><img src="<?php echo $this->assetBaseurl.'back-bt-tokopedia.png' ?>" alt="" class="img img-fluid"></a></li>
                            <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['contact_bukalapak'] ?>"><img src="<?php echo $this->assetBaseurl.'back-bt-bukalapak.png' ?>" alt="" class="img img-fluid"></a></li>
                        </ul>
                        <div class="clear"></div>
                        <p>or CHAT WITH us to inquire &nbsp;<img src="<?php echo $this->assetBaseurl.'wa-small-logo.png' ?>" alt="" class="img img-fluid">&nbsp; 081 650 85858</p>

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="box-mx_middles mobiles d-block d-sm-none">
                    <div class="pict"><img src="<?php echo $this->assetBaseurl ?>pict-nabout-products_lnd.png" alt="" class="img img-fluid w-100"></div>
                    <div class="ins_text">
                        <h5>GET YOUR SALTED EGG FISH SKIN NOW!</h5>
                        <div class="clear"></div>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['contact_tokopedia'] ?>"><img src="<?php echo $this->assetBaseurl.'back-bt-tokopedia.png' ?>" alt="" class="img img-fluid"></a></li>
                            <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['contact_bukalapak'] ?>"><img src="<?php echo $this->assetBaseurl.'back-bt-bukalapak.png' ?>" alt="" class="img img-fluid"></a></li>
                        </ul>
                        <div class="clear"></div>
                        <p>or CHAT WITH us to inquire &nbsp;<img src="<?php echo $this->assetBaseurl.'wa-small-logo.png' ?>" alt="" class="img img-fluid">&nbsp; 081 650 85858</p>

                        <div class="clear"></div>
                    </div>
                </div>
                <div class="py-3"></div>

            </div>


            <div class="clear"></div>
        </div>
    </section>
</section>