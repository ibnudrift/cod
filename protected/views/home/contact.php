
<section class="outer_wrapper_inside">
    <section class="about-snt-1">
        <div class="tops_page_title">
            <h1>CONTACT US</h1>
        </div>

        <div class="py-4"></div>
        <div class="py-2"></div>

        <div class="prelatife container">
            <div class="inners">
                <div class="content-texts text-center mx-auto mw845">
                    <!-- <h2>WHAT SETS US APART<br>FROM ANOTHER FRIED FISH SKIN PRODUCT</h2> -->
                    <img src="<?php echo $this->assetBaseurl ?>txt-title-location.png" alt="" class="img img-fluid">
                    <p>&nbsp;</p>

                    <div class="py-2"></div>

                    <div class="contact_call_wa text-center">
                        <img src="<?php echo $this->assetBaseurl ?>wa-icon-contact.png" alt="" class="img img-fluid mx-auto text-center d-block">
                        <div class="clear py-2"></div>
                        <p class="m-0">
                            <a href="https://wa.me/<?php echo str_replace('08', '628', str_replace(' ', '', $this->setting['contact_wa'])); ?>">Click To Chat on Whatsapp<br>
                            <strong><?php echo $this->setting['contact_wa']; ?></strong></a>
                        </p>
                    </div>
                    <div class="py-4"></div>
                    <div class="py-2"></div>

                    <div class="address_contact_dn">
                        <div class="row no-gutters">
                            <div class="col-md-10"></div>
                            <div class="col-md-40">
                                <div class="row">
                                    <div class="col">
                                        <div class="n_texts">
                                            <span><?php echo $this->setting['contact_address_title_1'] ?></span>
                                            <div class="py-2"></div>
                                            <?php echo $this->setting['contact_address_1'] ?>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="n_texts">
                                            <span><?php echo $this->setting['contact_address_title_2'] ?></span>
                                            <div class="py-2"></div>
                                            <?php echo $this->setting['contact_address_2'] ?>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="col-md-10"></div>
                        </div>
                    </div>


                    <div class="py-5"></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>

</section>

<style type="text/css">
    section.bottoms_home_block_pop{
        background: url('<?php echo $this->assetBaseurl ?>back_home_sects_3_full_white.jpg');
    }
</style>