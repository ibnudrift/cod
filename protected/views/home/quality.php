<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>ill-about.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Quality & Process</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Quality & Process</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-sec-quality-1">
    <div class="prelative container">
        <div class="row list_data-1">
            <div class="col-md-30">
                <div class="pictures">
                    <img src="<?php echo $this->assetBaseurl; ?>Rectangle-7-copy-21.jpg" alt="" class="img img-fluid">
                </div>
            </div>
            <div class="col-md-30">
                <div class="content content-texts pt-5">
                    <h3>In BMI, Quality Is In Each Step Of The Production Processing</h3>
                    <h5>Quality control process at PT Bumi Menara Internusa Machines Industry starts at the very beginning. For our shrimp customers, we documented each step of the process starting from the commodity materialising, preparation and processing until delivery.</h5>
                    <p>The production and processing at BMI uses smart and efficient technology powered by SAP so that we can bring better transparency to our customers. We have the biggest passion to be the best in the industry, therefore we are aware and willing to invest on quality control department.</p>
                    <p>Our quality teams will stop at nothing to ensure that each process are done with quality for customer benefit. Every fine detail is studied meticulously.</p>
                </div>
            </div>
        </div>
        <div class="row list_data-2">
            <div class="col-md-30">
                <div class="content content-texts pt-5">
                    <h3>Quality Sourcing</h3>
                    <h5>We consult, educate shrimp farmers and even treat the pond with necessary treatments to ensure farmers can grow the best commodity. We also invest in building our own shrimp ponds to supply the never ending demands.</h5>
                    <p>For other commodities such as fish, crabs and other sea food products, we are meticulous with the origin of the commodity, how they were bred, how they were harvested, and how they were delivered to our facility. We are concerned with not just about how fresh the commodities are, but we are taking attention towards animal cruelty, crimes against the environment, the preservation of nature and fauna. We’re aiming for profit but we care more on how we could help prevent the earth from disaster, and how we could preserve for our children.</p>
                </div>
            </div>
            <div class="col-md-30">
                <div class="pictures">
                    <img src="<?php echo $this->assetBaseurl; ?>Rectangle-7-copy-21.jpg" alt="" class="img img-fluid">
                </div>
            </div>
        </div>
        <div class="row list_data-3">
            <div class="col-md-30">
                <div class="pictures">
                    <img src="<?php echo $this->assetBaseurl; ?>Rectangle-7-copy-21.jpg" alt="" class="img img-fluid">
                </div>
            </div>
            <div class="col-md-30">
                <div class="content content-texts pt-5">
                    <h3>Quality Processing</h3>
                    <h5>We at PT Bumi Menara Internusa are always adapting and embracing “change” and “trends”, that’s why we have the deppest thirst when it comes to new technology. Anything that could maximise the outcome quality and outsmart the previous process will be our investment priority checklist. We always push ourselves far beyond our limit, we are hard to ourselves in order to bring ultimate satisfactory to our consumers.</h5>
                    <p>We care about our customers and we know how important to get clear trace of production progress - therefore we integrate our business process with the best enterprise technology to provide real-time tracking and provide product tracebility.</p>
                    <p>The continuous innovation in PT Bumi Menara Internusa is a never ending journey and it’s a way of life for us - Whether it’s the latest technology or radical ideas for product processing, we’re always pushing ourselves to do more, and do it better.</p>
                    <img src="<?php echo $this->assetBaseurl; ?>logo-sap.png" class="img img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="home-sec-quality-2 back-quality_outerbottom" >
    <div class="prelative container">
        <div class="py-5"></div>
        <div class="py-4 d-none d-sm-block"></div>
        <div class="inner-content content-texts text-center">
            <div class="row">
                <div class="col-md-10"></div>
                <div class="col-md-40">
                    <h3>We Go Extra Miles For Quality & Excellence</h3>
                    <h5>PT. Bumi Menara Internusa believe that our customers deserves only the best, and by doing that so, we will gain trust and lasting bond - a long term business relationship.</h5>
                </div>
                <div class="col-md-10"></div>
            </div>
        </div>

        <div class="box-image">
            <h3>The BMI quality process commitment:</h3>
            <div class="row lists_data">
                <div class="col-md-15 col-30">
                    <div class="pictures">
                        <img src="<?php echo $this->assetBaseurl; ?>Layer-41.png" class="img img-fluid" alt="">
                    </div>
                    <div class="contents1">
                        <h4>Best Raw Commodity</h4>

                    </div>
                </div>
                <div class="col-md-15 col-30">
                    <div class="pictures">
                        <img src="<?php echo $this->assetBaseurl; ?>Layer-391.png" class="img img-fluid" alt="">
                    </div>
                    <div class="contents1">
                        <h4>Best QC Team</h4>

                    </div>
                </div>
                <div class="col-md-15 col-30">
                    <div class="pictures">
                        <img src="<?php echo $this->assetBaseurl; ?>Layer-40.png" class="img img-fluid" alt="">
                    </div>
                    <div class="contents1">
                        <h4>Best Testing Facility</h4>

                    </div>
                </div>
                <div class="col-md-15 col-30">
                    <div class="pictures">
                        <img src="<?php echo $this->assetBaseurl; ?>Layer-421.png" class="img img-fluid" alt="">
                    </div>
                    <div class="contents1">
                        <h4>Best Enterprise Technology</h4>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>