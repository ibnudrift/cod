
<section class="outer_wrapper_inside">
    <section class="about-snt-1">
        <div class="tops_page_title">
            <h1>LOCATION WILDSKIN</h1>
        </div>

        <div class="py-4"></div>
        <div class="py-2"></div>

        <?php 
        $criteria = new CDbCriteria;
        $criteria->group = 'prov';
        $criteria->order = 't.id ASC';
        $provinces = Address::model()->findAll($criteria);

        $criteria = new CDbCriteria;
        $criteria->group = 'kota';
        $criteria->order = 't.id ASC';
        $kotas = Address::model()->findAll($criteria);
        ?>

        <div class="prelatife container">
            <div class="inners">
                <div class="content-texts text-center mx-auto mw845">
                    <!-- <h2>WHAT SETS US APART<br>FROM ANOTHER FRIED FISH SKIN PRODUCT</h2> -->
                    <img src="<?php echo $this->assetBaseurl ?>txt-title-location.png" alt="" class="img img-fluid">
                    <p>&nbsp;</p>

                    <div class="py-2"></div>
                    <div class="box_inlins_form_location mx-auto text-center">
                        <form class="form-inline" method="get" action="">
                          <label class="mr-3">Your Location:</label>
                          <select name="province" id="" class="form-control mr-3">
                              <option value="">Select Province</option>
                              <?php foreach ($provinces as $key => $value): ?>
                                  <option <?php if (strtolower($value->prov) == $_GET['province']): ?>selected="selected"<?php endif ?> value="<?php echo strtolower($value->prov) ?>"><?php echo $value->prov ?></option>
                              <?php endforeach ?>
                          </select>

                          <select name="city" id="" class="form-control mr-3">
                              <option value="">Select City</option>
                              <?php foreach ($kotas as $key => $value): ?>
                                  <option <?php if (strtolower($value->kota) == $_GET['city']): ?>selected="selected"<?php endif ?> value="<?php echo strtolower($value->kota) ?>"><?php echo $value->kota ?></option>
                              <?php endforeach ?>
                          </select>

                          <button type="submit" class="btn btn-primary">SEARCH NOW</button>
                        </form>
                        <div class="clear"></div>
                    </div>

                    <div class="py-4"></div>
                    <div class="lines-grey"></div>
                    <div class="py-5"></div>

                    <?php 
                    $criteria = new CDbCriteria;
                    $criteria->group = 'prov';
                    $criteria->order = 't.id ASC';
                    if (isset($_GET['province']) && $_GET['province'] != '') {
                        $criteria->addCondition('prov = :nprov');
                        $criteria->params[':nprov'] = $_GET['province'];
                    }
                    $provinces2 = Address::model()->findAll($criteria);
                    ?>
                    <div class="list_locations">
                        <?php foreach ($provinces2 as $key => $value): ?>
                        <div class="n_list pb-5 mb-2">
                            <h4><?php echo strtoupper($value->kota) ?></h4>
                            <div class="clear py-2"></div>
                            <?php 
                            $criteria = new CDbCriteria;
                            $criteria->order = 't.id DESC';
                            if (isset($_GET['city']) && $_GET['city'] != '') {
                                $criteria->addCondition('kota = :ncity');
                                $criteria->params[':ncity'] = $_GET['city'];
                            }else{
                                $criteria->addCondition('kota = :nkota');
                                $criteria->params[':nkota'] = $value->kota;
                            }

                            $kotas_sub = Address::model()->findAll($criteria);
                            ?>
                            <div class="lits_ndata">
                                <div class="row justify-content-center">
                                    <?php foreach ($kotas_sub as $keys => $values): ?>
                                    <div class="col-30 col-md-15">
                                        <p><strong><?php echo $values->nama ?></strong> <br>
                                        <?php echo nl2br($values->address_1) ?></p>
                                    </div>
                                    <?php endforeach ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>

                        <div class="clear"></div>
                    </div>

                    <div class="py-5"></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>

</section>

<style type="text/css">
    section.bottoms_home_block_pop{
        background: url('<?php echo $this->assetBaseurl ?>back_home_sects_3_full_white.jpg');
    }
</style>