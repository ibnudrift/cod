
<section class="outer_wrapper_inside">
    <section class="about-snt-1">
        <div class="tops_page_title">
            <h1>trace your wildskin</h1>
        </div>

        <div class="py-4"></div>
        <div class="py-2"></div>

        <?php 
        $origins = htmlspecialchars($_GET['origin']);
        $model = Traces::model()->find('origin = :origin', array(':origin'=> $origins));
        ?>

        <div class="prelatife container">
            <div class="inners">
                <?php if ($model): ?>
                <div class="content-texts text-center mx-auto blocks_traces_desc">
                    <!-- <h2>WHAT SETS US APART<br>FROM ANOTHER FRIED FISH SKIN PRODUCT</h2> -->
                    <img src="<?php echo $this->assetBaseurl ?>txt-title-tracespage.png" alt="" class="img img-fluid">
                    <p>&nbsp;</p>

                    <div class="py-3"></div>

                    <div class="mw845 mx-auto">
                        <div class="row nlist_origin">
                            <div class="col-30 col-md-15">
                                <div class="s_texts">
                                    <p>ORIGIN</p>
                                    <p><strong><?php echo strtoupper($model->origin) ?></strong></p>
                                </div>
                            </div>
                            <div class="col-30 col-md-15 data2">
                                <div class="s_texts">
                                    <p>FISHING COMPANY</p>
                                    <p><strong><?php echo strtoupper($model->company) ?></strong></p>
                                </div>
                            </div>
                            <div class="col-30 col-md-15">
                                <div class="s_texts">
                                    <p>DATE PROCESSED</p>
                                    <p><strong><?php echo date("d-m-Y", strtotime($model->date_process)); ?></strong></p>
                                </div>
                            </div>
                            <div class="col-30 col-md-15 lasts">
                                <div class="s_texts">
                                    <p>DATE DISTRIBUTED</p>
                                    <p><strong><?php echo date("d-m-Y", strtotime($model->date_distribution)); ?></strong></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="py-3"></div>
                    <p class="mb-0">The food you’re consuming were came from a wild Cod on the natural environment to ensure sustainability of the fish population.</p>
                    
                    <div class="py-4"></div>
                    <div class="py-3"></div>

                    <div class="tx_instagram_big">
                        <a target="_blank" href="<?php echo 'https://instagram.com/'.$this->setting['url_instagram'] ?>">
                            <i class="fa fa-instagram"></i>
                            <br>
                            @<?php echo $this->setting['url_instagram'] ?>
                        </a>
                    </div>

                    <div class="py-4"></div>
                    <div class="py-3"></div>
                    <div class="clear"></div>
                </div>
                <?php endif ?>

            </div>
        </div>
    </section>

</section>

<style type="text/css">
    section.bottoms_home_block_pop{
        background: url('<?php echo $this->assetBaseurl ?>back_home_sects_3_full_white.jpg');
    }
</style>