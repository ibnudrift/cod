<section class="backs_home_sect1">
  <div class="prelatife container py-4">
    <div class="inners py-5">

      <div class="topsn_lett text-center d-block">
        <?php echo $this->setting['home1_title'] ?>
        <div class="py-2"></div>
        <img src="<?php echo $this->assetBaseurl ?>sub-skin-home1.png" alt="" class="img img-fluid d-block mx-auto">
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <div class="py-3"></div>
      <div class="row content-texts">
        <div class="col-md-30">
          <?php echo $this->setting['home1_left_content'] ?>
        </div>
        <div class="col-md-30">
          <?php echo $this->setting['home1_right_content'] ?>
        </div>
      </div>
      
      <div class="py-3"></div>
      
      <div class="clear clearfix"></div>
    </div>
  </div>

  <div class="pos_middle_abs_feat d-block mx-auto text-center">
    <img src="<?php echo $this->assetBaseurl ?>pict_humbmiddle.png" alt="" class="img img-fluid">
  </div>
</section>

<section class="back_home_sect2 py-5">
  <div class="prelatife container">
    <div class="inners py-5">
      <div class="py-5"></div>
      
      <div class="lists_sect2_home">
        <div class="items">
          <div class="row no-gutters">
            <div class="col-md-30">
              <div class="picture"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['home2_banner_pict'] ?>" alt="" class="img img-fluid"></div>
            </div>
            <div class="col-md-30 back-gold">
              <div class="descriptions">
                <div class="texts">
                  <?php echo $this->setting['home2_banner_content'] ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/location')); ?>" class="btn btn_customs_default">find wildskin</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="items">
          <div class="row no-gutters">
            <div class="col-md-30 back-grey order-2 order-sm-1">
              <div class="descriptions">
                <div class="texts">
                  <?php echo $this->setting['home3_banner_content'] ?>
                  <a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>" class="btn btn_customs_default">INQUIRE NOW</a>
                </div>
              </div>
            </div>
            <div class="col-md-30 order-1 order-sm-2">
              <div class="picture"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['home3_banner_pict'] ?>" alt="" class="img img-fluid"></div>
            </div>
          </div>
        </div>
        
        <div class="clear"></div>
      </div>   
      
      <div class="clear"></div>
    </div>
  </div>
</section>
