<div class="h98"></div>

<section class="inside_cover_pg">
    <div class="pictures prelatife"><img src="<?php echo $this->assetBaseurl; ?>ill-about.jpg" alt="" class="img img-fluid"></div>
    <div class="blocks_outer_breadstop">
        <div class="prelatife container">
            <div class="row">
                <div class="col-md-25">
                    <div class="blocks_title prelatife">
                        <div class="inner">
                            <h1>Products</h1>
                        </div>
                    </div>
                </div>
                <div class="col-md-35">
                    <div class="text-right py-2 my-2 right_block">
                        <div class="d-inline-block align-middle">
                            <nav aria-label="breadcrumb">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Products</li>
                              </ol>
                            </nav>
                        </div>
                        <div class="d-inline-block align-middle px-2">
                        <img src="<?php echo $this->assetBaseurl; ?>vert-liner.jpg" alt="" class="img img-fluid">
                        </div>
                        <div class="d-inline-block align-middle sback">
                            <a href="#">back</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

 <section class="home-sec-products">
   <div class="prelatife container">
        <div class="row">
            <div class="col-md-60">
                 <div class="inner-judul">
                   <h3>Our Seafood Products</h3>
                   <p>PT. Bumi Menara Internusa is supplying a wide range of shrimp, fish, crabs and value added seafood products from Indonesia, including raw and cooked sea food products such as White Shrimp, Squid, Cuttlefish, Baby Octopus, Seafood Mix, Seafood Skewers, Tuna Loins and many more.</p>
                 </div>
             </div>
        </div>
        <div class="py-4"></div>
        <?php 
        $dn_productlists = [
                                [
                                    'pict'=>'Layer-47.jpg',
                                    'titles'=>'Shrimp',
                                ],
                                [
                                    'pict'=>'Layer-48.jpg',
                                    'titles'=>'Alaskan Salmon',
                                ],
                                [
                                    'pict'=>'Layer-49.jpg',
                                    'titles'=>'Yellow Fin Tuna',
                                ],
                                [
                                    'pict'=>'Layer-50.jpg',
                                    'titles'=>'Crap',
                                ],
                                [
                                    'pict'=>'Layer-47.jpg',
                                    'titles'=>'Baby Octopus',
                                ],
                                [
                                    'pict'=>'Layer-48.jpg',
                                    'titles'=>'Cuttlefish',
                                ],
                                [
                                    'pict'=>'Layer-49.jpg',
                                    'titles'=>'Squid',
                                ],
                                [
                                    'pict'=>'Layer-50.jpg',
                                    'titles'=>'Other Seafood Products',
                                ],
                            ];
        ?>
        <div class="outers_list_product">

            <div class="row">
                <?php foreach ($dn_productlists as $key => $value): ?>
                <div class="col-md-15 col-30">
                    <div class="items_box">
                        <div class="picture">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail')); ?>">
                                <img src="<?php echo $this->assetBaseurl ?><?php echo $value['pict'] ?>" alt="" class="img img-fluid">
                            </a>
                        </div>
                        <div class="info">
                            <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail')); ?>"><h3><?php echo $value['titles'] ?></h3></a>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>

            </div>
        </div>
        <div class="py-3"></div>
        <div class="clear"></div>
    </div>
</section>