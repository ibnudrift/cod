
<section class="outer_wrapper_inside">
    <section class="about-snt-1">
        <div class="tops_page_title">
            <h1>snap and share</h1>
        </div>

        <div class="py-4"></div>
        <div class="py-2"></div>
        
        <div class="inners_outer_section snap_page">
            <div class="prelatife container">
                <div class="inners">
                    <div class="content-texts text-center mx-auto blocks_traces_desc">
                        <!-- <h2>WHAT SETS US APART<br>FROM ANOTHER FRIED FISH SKIN PRODUCT</h2> -->
                        <img src="<?php echo $this->assetBaseurl ?>txt-title-snapshare.png" alt="" class="img img-fluid">
                        <p>&nbsp;</p>

                        <div class="py-3"></div>

                        <?php echo $this->setting['share_content'] ?>
                        
                        <div class="py-4"></div>
                        <div class="py-3"></div>

                        <div class="tx_instagram_big">
                            <a target="_blank" href="<?php echo 'https://instagram.com/'.$this->setting['url_instagram'] ?>">
                                <i class="fa fa-instagram"></i>
                                <br>
                                @<?php echo $this->setting['url_instagram'] ?>
                            </a>
                        </div>

                        <div class="py-4"></div>
                        <div class="py-3"></div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>

    </section>

</section>

<style type="text/css">
    section.bottoms_home_block_pop{
        background: url('<?php echo $this->assetBaseurl ?>back_home_sects_3_full_white.jpg');
    }
</style>