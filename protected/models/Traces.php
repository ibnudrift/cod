<?php

/**
 * This is the model class for table "tb_traces".
 *
 * The followings are the available columns in table 'tb_traces':
 * @property string $id
 * @property string $kode
 * @property string $origin
 * @property string $company
 * @property string $date_process
 * @property string $date_distribution
 * @property string $date_input
 */
class Traces extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Traces the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_traces';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode, origin, company', 'length', 'max'=>225),
			array('date_process, date_distribution, date_input', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, kode, origin, company, date_process, date_distribution, date_input', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'kode' => 'Kode',
			'origin' => 'Origin',
			'company' => 'Company',
			'date_process' => 'Date Process',
			'date_distribution' => 'Date Distribution',
			'date_input' => 'Date Input',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('kode',$this->kode,true);
		$criteria->compare('origin',$this->origin,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('date_process',$this->date_process,true);
		$criteria->compare('date_distribution',$this->date_distribution,true);
		$criteria->compare('date_input',$this->date_input,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}